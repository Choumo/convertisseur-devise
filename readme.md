# Convertisseur de devise

## Frameworks
Non

## Technologies 
- Javascript
- HTML
- Tailwindcss
- Daisyui
- Freecurrency api

## Installation

```bash
$ git clone https://gitlab.com/Choumo/convertisseur-devise
$ cd convertisseur-devise
$ npm install 
```

mettre la clé api dans env.js

## Tests
### Vitest
```bash
$ npm run test
```
### Playwright
```bash
$ npx playwright test
```