import {env} from "../env";
import { http, HttpResponse } from 'msw'
import {currencyConvertObject, currencyListObject} from "./mockObject";

export const handlers = [
    http.get("https://api.freecurrencyapi.com/v1/currencies?apikey=" + env.api_key, () => {
        return HttpResponse.json(currencyListObject);
    }),
    http.get("https://api.freecurrencyapi.com/v1/latest?base_currency=EUR&currencies=USD&apikey=" + env.api_key, () => {
        return HttpResponse.json(currencyConvertObject);
    }),
]