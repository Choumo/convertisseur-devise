// @ts-check
const { test, expect } = require('@playwright/test');

const url = ""; // A changer avec url du fichier (moi c'est avec serveur local webstorm)

test("convert devise", async ({ page }) => {
  await page.goto('http://localhost:5173/');

  const baseSelect = await page.locator('#baseDevise');
  const targetSelect = await page.locator('#targetDevise');
  await expect(baseSelect).toBeVisible();
  await expect(targetSelect).toBeVisible();

  const baseDefaultOption = await baseSelect.locator('option[disabled]');
  const targetDefaultOption = await targetSelect.locator('option[disabled]');

  await targetDefaultOption.click();
  await expect(baseDefaultOption).toBeVisible();

  await baseDefaultOption.click();
  await expect(targetDefaultOption).toBeVisible();

  const baseOptions = await baseSelect.locator('option');
  const targetOptions = await targetSelect.locator('option');
  await expect(baseOptions.count()).toBeGreaterThan(1);
  await expect(targetOptions.count()).toBeGreaterThan(1);

  await page.selectOption('#baseDevise', 'EUR');
  await page.selectOption('#targetDevise', 'USD');
  await page.fill('#montant', '100');
  await page.click('button');
  await page.waitForSelector('#resultat');

  const result = await page.inputValue('#resultat');
  expect(parseFloat(result)).toBeGreaterThan(0);
})