import { expect, describe, test } from 'vitest'
import { getCurrencies, convertDevise } from '../../currency';
import {server} from "../../mocks/node";
import {currencyConvertObject, currencyListObject} from "../../mocks/mockObject";

server.listen();

describe('currency', () => {
    test("get currency list", async () => {
        const currencyList = await getCurrencies();
        expect(currencyList).toMatchObject(currencyListObject);
    });

    test("convert currency list", async () => {
        const currencyList = await convertDevise("EUR", "USD");
        expect(currencyList).toMatchObject(currencyConvertObject);
    });
})
