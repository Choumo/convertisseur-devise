// Fonction pour récupérer la liste des devises depuis l'API
import { env } from "./env.js";
export async function getCurrencies() {
    const response = await fetch("https://api.freecurrencyapi.com/v1/currencies?apikey=" + env.api_key, {
        method: "GET",
    });

    if(!response.ok) {
        alert("Une erreur est survenenue lors de l'appel à l'API");
    }

    return response.json();
}

// Fonction pour reécupérer un montant converti dans une devise grâce à l'API
export async function convertDevise(baseDevise = "EUR", targetDevise = "USD") {
    const response = await fetch("https://api.freecurrencyapi.com/v1/latest?base_currency=" + baseDevise + "&currencies=" + targetDevise + "&apikey=" + env.api_key, {
        method: "GET",
    });

    if(!response.ok) {
        alert("Une erreur est survenenue lors de l'appel à l'API");
    }

    return response.json();
}